class Document < ActiveRecord::Base
  mount_uploader :pdf, PdfUploader

  ### CALLBACKS ###
  before_create :set_defaults

  ### VALIDATIONS ###
  validates :pdf, presence: true
  validates :category, presence: true

  ### RELATIONS ###
  belongs_to :category

  def set_defaults
    url = self.pdf.url
    uri = URI.parse(url)
    self.description = File.basename(uri.path, ".*" )
    self.title = File.basename(uri.path, ".*" )
  end

  def self.search(query)
    where('description like ?', "%#{query}%")
  end
end