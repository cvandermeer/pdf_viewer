class Category < ActiveRecord::Base
  has_ancestry

  ### RELATIONS ###
  has_many :documents
end
