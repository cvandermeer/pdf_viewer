class CategoriesController < ApplicationController
  before_action :check_admin, only: [:new, :create, :update, :destroy, :edit]
  before_action :set_category, only: [:edit, :update, :destroy, :show]

  def new
    @category = Category.new
    @title = params[:title]
  end

  def edit
  end

  def show
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to @category, notice: 'Categorie aangemaakt!'
    else
      render :new
    end
  end

  def update
    if @category.update(category_params)
      redirect_to documents_path, notice: 'Categorie aangepast!'
    else
      render :edit
    end
  end

  private
    def category_params
      params.require(:category).permit(:title, :parent_id)
    end

    def set_category
      @category = Category.find(params[:id])
    end

    def check_admin
      if !current_user.admin
        redirect_to root_path, alert: 'U bent niet gemachtigd!'
      end
    end
end
