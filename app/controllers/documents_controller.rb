class DocumentsController < ApplicationController
  before_action :set_document, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :check_admin, only: [:new, :create, :update, :destroy, :edit]

  def index
    if params[:search]
      @documents = Document.search(params[:search])
      render json: @documents
    else
      @documents = Document.all
    end
  end

  def show
  end

  def new
    @document = Document.new
  end

  def create
    @document = Document.new(document_params)
    if @document.save
      redirect_to @document.category, notice: 'Bestand geupload!'
    else
      render :new, alert: 'Uploaden mislukt!'
    end
  end

  def edit
  end

  def update
    if @document.update(document_params)
      redirect_to documents_path, notice: 'Bestand aangepast!'
    else
      render :edit, alert: 'Aanpassen mislukt!'
    end
  end

  def destroy
    @document.destroy
    redirect_to documents_path, notice: 'Bestand verwijderd!'
  end

  private
    def document_params
      params.require(:document).permit(:title, :description, :pdf, :category_id)
    end

    def set_document
      @document = Document.find(params[:id])
    end

    def check_admin
      if !current_user.admin
        redirect_to root_path, alert: 'U bent niet gemachtigd!'
      end
    end
end