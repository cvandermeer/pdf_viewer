var ready;
var searchFormVal;
ready = function(){

  $('.jsFindDocument').on('keyup', function(){
    searchFormVal = $('.jsSearchForm').val();
    setTimeout(function(){
      if(searchFormVal.length > 1){
        $('.jsFindDocument').submit();
      }
    }, 200);
  });

  $('.jsFindDocument').on('ajax:success', function(evt, post, status){
    $('.jsFoundDocuments').html('');
    searchFormVal = $('.jsSearchForm').val();
    if(post.length) {
      for(i = 0; i < post.length; i++){
        if(i < 10) {
          $('.jsFoundDocuments').append('<p><a href="/documents/' + post[i].id + '">' + post[i].title + '</a></p>');
        } else {
          return false;
        }
      }
    } else {
      $('.jsFoundDocuments').append('<li>Geen documenten gevonden met: ' + searchFormVal + '</li>');
    }
  });


}

$(document).ready(ready);
$(document).on('page:load', ready)